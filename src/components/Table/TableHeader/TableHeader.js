import React, { Component } from 'react';
import { array, object } from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';

import styles from './styles';

const uuidv4 = require('uuid/v4');

class TableHeader extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { tableHeader, headerWidth, classes } = this.props;
    return (
      <TableHead className={classes.root}>
        <TableRow>
          {tableHeader.map(
            (item, idx) => (
              <TableCell
                className={classes.tableCell}
                key={uuidv4()}
                align="center"
                padding="checkbox"
                width={headerWidth[idx]}
              >
                {item}
              </TableCell>
            ),
            this,
          )}
        </TableRow>
      </TableHead>
    );
  }
}

TableHeader.propTypes = {
  classes: object.isRequired,
  tableHeader: array.isRequired,
  headerWidth: array.isRequired,
};

export default withStyles(styles)(TableHeader);
