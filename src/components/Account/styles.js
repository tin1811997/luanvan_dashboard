const styles = theme => ({
  card: {
    width: '100%',
    borderRadius: 0,
  },
  actions: {
    display: 'flex',
    height: '48px',
    backgroundColor: '#FFFFFF',
    color: '#666666',
    cursor: 'pointer',
  },
  expand: {
    transform: 'rotate(180deg)',
    marginLeft: 'auto',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
  },
  expandOpen: {
    transform: 'rotate(0deg)',
    marginTop: '4px',
  },
  avatar: {
    textTransform: 'uppercase',
    width: '30px',
    height: '30px',
    fontSize: '16px',
    backgroundColor: '#666666',
  },
  userName: {
    marginLeft: '8px',
    whiteSpace: 'nowrap',
    width: '100%',
    overflow: 'hidden',
    textOverflow: 'ellipsis',
  },
  collapse: {
    position: 'relative',
  },
  optionRow: {
    height: '40px',
    lineHeight: '40px',
    textAlign: 'center',
    fontWeight: '550',
    fontSize: '16px',
    color: '#666666',
    backgroundColor: '#FFFFFF',
    border: '0.5px solid #CCCCCC',
    cursor: 'pointer',
    '&:hover': {
      backgroundColor: '#E5E5E5',
    },
  },
});

export default styles;
