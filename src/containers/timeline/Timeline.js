import React, { Component } from 'react';
import { object } from 'prop-types';
import { withRouter } from 'react-router-dom';
import NavBar from '../../components/navBar/NavBar';
import TableComponent from '../../components/Table/TableComponent/TableComponent';
import TimelineAPI from '../../apis/Timeline'

import './timline.scss';

class Timeline extends Component {
  constructor(props) {
    super(props);
    this.state = {
      timline: []
    };
  }

  componentDidMount() {
    this.getAllTimeline();
  }

  renderColName = value => (
    <div className="name-wrap name-wrap__course">
      {value}
    </div>
  );
  renderCheck = value => (
    <div className="name-wrap">
      {value}
    </div>
  );

  getAllTimeline = async () => {
    let res = await TimelineAPI.getAllTimeline(localStorage.getItem('id'));
    this.setState({
      timline: res.data.course
    });
  };

  getDatatable = () => {
    const { timline } = this.state;
    return {
      colums: [
        {
          label: '',
          name: 'course',
          width: '6.25%',
        },
        {
          label: 'Monday',
          name: 'monday',
          width: '6.25%',
        },
        {
          label: 'Tuesday',
          name: 'tuesday',
          width: '6.25%',
        },
        {
          label: 'Wednesday',
          name: 'wednesday',
          width: '6.25%',
        },
        {
          label: 'Thursday',
          name: 'thursday',
          width: '6.25%',
        },
        {
          label: 'Friday',
          name: 'friday',
          width: '6.25%',
        },
        {
          label: 'Saturday',
          name: 'saturday',
          width: '6.25%',
        },
        {
          label: 'Sunday',
          name: 'sunday',
          width: '6.25%',
        }
      ],
      rows: timline.map(row => ({
        ...row,
        course: this.renderColName(row.course),
        monday: this.renderCheck(row.monday),
        tuesday: this.renderCheck(row.tuesday),
        wednesday: this.renderCheck(row.wednesday),
        thursday: this.renderCheck(row.thursday),
        friday: this.renderCheck(row.friday),
        saturday: this.renderCheck(""),
        sunday: this.renderCheck(""),
      }))
      
    }
  }

  render() {
    const dataTable = this.getDatatable();
    return (
      <NavBar titleHeader="Schedules">
        <div className="timeline-container">
          <div style={{ margin: '21px 28px' }}>
            <TableComponent dataTable={dataTable} />
          </div>
        </div>
      </NavBar>
    );
  }
}

Timeline.propTypes = {
  history: object.isRequired,
};

export default withRouter(Timeline);