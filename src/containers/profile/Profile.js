import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import './profile.scss'
import avatar from '../../assets/img/avatar.png'
import NavBar from '../../components/navBar/NavBar';

class Profile extends Component {
  constructor(props) {
    super(props);
    this.state = {
    }
  };


  render() {
    return (
      <NavBar titleHeader="Profile">
        <div className="profile">
          <div className="profile__avatar">
            <div className="img-avatar-wrapper">
              <img src={avatar} alt="avatar" className="img-avatar"></img>
            </div>
          </div>
          <div className="profile__content">
            <p>
              <strong>Student ID :</strong> 123456
            </p>
            <p>
              <strong>Name :</strong> John Smith
            </p>
            <p>
              <strong>School Year :</strong> 2015 - 2019
            </p>
            <p>
              <strong>Date of Birth :</strong> 1997 - 11 -21
            </p>
            <p>
              <strong>Sex :</strong>  Male
            </p>
            <p>
              <strong>My Phone :</strong> 093 999999
            </p>
            <p>
              <strong>Parent Phone</strong> 093 999999
            </p>
          </div>
        </div>
      </NavBar>
    )
  }
}

export default withRouter(Profile);