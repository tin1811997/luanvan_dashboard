const AuthenticationService = {
  removeToken: () => {
    localStorage.removeItem('token');
    localStorage.removeItem('user');
    window.location.href = '/';
  },
};

export default AuthenticationService;
