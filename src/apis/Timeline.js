import HTTPService from '../services/HTTPServices';

const TimelineAPI = {
    getAllTimeline: (studentId) => HTTPService.sendRequest('get', '/courses/coursestime/' + studentId)
};

export default TimelineAPI;
