const drawerWidth = 224;

const styles = theme => ({
  root: {
    display: 'flex',
    overflow: 'hidden',
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  appBarShift: {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  menuButton: {
    marginRight: 36,
    padding: 16,
  },
  titleHeader: {
    color: '#ffffff',
    width: '100%',
    textAlign: 'center',
  },
  hide: {
    display: 'none',
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
    whiteSpace: 'nowrap',
  },
  drawerOpen: {
    width: drawerWidth,
    overflow: 'hidden',
    border: 'none',
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  drawerClose: {
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    overflowX: 'hidden',
    width: '56px',
    overflow: 'hidden',
  },
  toolbar: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
    padding: '0 8px',
    backgroundColor: '#fff',
    border: 'none',
    minHeight: 56,
  },
  itemMenu: {
    display: 'flex',
    alignItems: 'center',
    fontSize: '15px',
    textDecoration: 'none',
    color: 'rgba(0, 0, 0, 0.74)',
  },
  icon: {
    padding: '0 20px 0 4px',
    fontSize: 20,
  },
  content: {
    flexGrow: 1,
    padding: '72px 0 16px 16px',
    paddingLeft: 0,
    width: '100%',
    overflowX: 'auto',
  },
  drawerAccount: {
    left: 0,
    bottom: 0,
    width: '100%',
    position: 'absolute',
    transition: 'all 0.5s',
    paddingLeft: '4px',
  },
  imgLogo: {
    display: 'block',
    paddingLeft: '1rem',
    height: '45px',
  },
});

export default styles;
