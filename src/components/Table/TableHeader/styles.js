const styles = () => ({
  root: {
    background: 'rgb(48, 117, 169)',
  },
  tableCell: {
    fontFamily: 'Roboto',
    color: '#ffffff',
    fontStyle: 'normal',
    fontWeight: 'bold',
    fontSize: '14px',
    lineHeight: 'normal',
  },
});

export default styles;
