import React, { Component } from 'react';
import { array, string } from 'prop-types';
import { Table } from 'antd';

import './StatisticalTable.scss';

class StatisticalTable extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { columns, data, title } = this.props;
    return (
      <div className="statistical-table-container">
        <div className="title-table">{title}</div>
        <Table
          columns={columns}
          dataSource={data}
          bordered
          scroll={{ x: 2024 }}
          pagination={false}
        />
      </div>
    );
  }
}

StatisticalTable.propTypes = {
  title: string.isRequired,
  columns: array,
  data: array,
};

StatisticalTable.defaultProps = {
  columns: [
    {
      title: 'No.',
      width: 90,
      dataIndex: 'numberOf',
      key: 'numberOf',
      align: 'center',
      fixed: 'left',
    },
    {
      title: 'Code',
      width: 100,
      dataIndex: 'code',
      key: 'code',
      align: 'center',
      fixed: 'left',
    },
    {
      title: 'Full name',
      width: 200,
      dataIndex: 'name',
      key: 'name',
      align: 'center',
      fixed: 'left',
    },
  ],
  data: [
    {
      numberOf: '01',
      code: '1560531',
      name: 'Nguyen Van A',
    },
  ]
}

export default StatisticalTable;
