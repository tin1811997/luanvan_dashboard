import Statistical from '../apis/statistical';
export const GET_STATISTICAL_SUCCESSFULL = 'counter/GET_STATISTICAL_SUCCESSFULL';
export const GET_STATISTICAL_FAILURE = 'counter/GET_STATISTICAL_FAILURE';

const initialState = {
  statisticals: [],
  lsClass: [],
};

const getOptions = (valueAction) => {
  let lsClass = [];
  valueAction.forEach(element => lsClass.push({ value: element.subject.id, label: element.subject.name }));
  return lsClass;
}

export default (state = initialState, action) => {
  switch (action.type) {
    case GET_STATISTICAL_SUCCESSFULL:
      return {
        ...state,
        statisticals: action.data,
        lsClass: getOptions(action.data),
      };
    case GET_STATISTICAL_FAILURE:
    default:
      return state;
  }
};

export const getAllStatistical = className => dispatch => (
  Statistical
    .getAllStatistical(className)
    .then((res) => {
      const { isError, data, err } = res;
      if (!isError) {
        dispatch({ type: GET_STATISTICAL_SUCCESSFULL, data: data.data });
      }
      return err;
    })
    .catch(error => dispatch({ type: GET_STATISTICAL_FAILURE, error }))
);
