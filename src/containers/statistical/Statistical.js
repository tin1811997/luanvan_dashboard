import React, { Component } from 'react';
import { func, array } from 'prop-types';
import { connect } from 'react-redux';
import range from 'lodash/range';
import uuidv4 from 'uuid/v4';
import { Check } from '@material-ui/icons';
import { Button } from 'antd';
import NavBar from '../../components/navBar/NavBar';
import StatisticalTable from '../../components/statisticalTable/StatisticalTable';
import SelectBox from '../../components/selectBox/SelectBox';

import { getAllStatistical } from '../../modules/statistical';

import './Statistical.scss';

const optionsClass =  [
  { value: '15CLC', label: '15 Chất Lượng cao' },
  { value: '16CLC', label: '16 Chất Lượng cao' },
  { value: '15TT', label: '15 Tiên Tiến' },
  { value: '16TT', label: '16 Tiên Tiến' },
];

const optionsClass_1 =  [
  { value: '15CLC', label: '15 Chất Lượng cao' }
];

class Statistical extends Component {
  constructor(props) {
    super(props);
    this.state = {
      classValue: {},
      subject: {},
    };
  }

  componentDidMount() {
    const { actionGetAllStatistical, lsClass } = this.props;
    actionGetAllStatistical('15CLC');
    if ( localStorage.getItem('role') === 'hocsinh'){
      this.setState({ classValue: optionsClass_1[0], subject: lsClass[0] });
    }
    else if (localStorage.getItem('role') === 'giaovu') {
      this.setState({ classValue: optionsClass[0], subject: lsClass[0] });
    }
  }

  renderIcon = (value) => {
    if (value) return <Check className="icon-check" />;
    return '';
  }

  renderColName = value => (
    <div className="name-wrap">
      {value}
    </div>
  );
  renderCheck = value => (
    <div className="icon-wrap">
      {this.renderIcon(value)}
    </div>
  );

  getCourses = () => {
    const { statisticals, lsClass } = this.props;
    const { subject } = this.state;
    const course =  subject || lsClass[0];
    const statistical = statisticals.find(data => data.subject.id === course.value);

    const columns = [
      {
        title: 'No.',
        width: 68,
        dataIndex: 'numberOf',
        rowKey: uuidv4(),
        align: 'center',
        fixed: 'left',
      },
      {
        title: 'Code',
        width: 96,
        dataIndex: 'code',
        rowKey: uuidv4(),
        align: 'center',
        fixed: 'left',
      },
      {
        title: 'Full name',
        width: 160,
        dataIndex: 'name',
        rowKey: uuidv4(),
        align: 'center',
        fixed: 'left',
      },
    ];

    range(0, 13).forEach((index) => {
      columns.push({
      title: `Week ${index}`,
      rowKey: uuidv4(),
      align: 'center',
      children: [
        {
          title: 'L1',
          dataIndex: `less${index}1`,
          rowKey: uuidv4(),
          width: 100,
          align: 'center',
        },
        {
          title: 'L2',
          dataIndex: `less${index}2`,
          rowKey: uuidv4(),
          width: 100,
          align: 'center',
        },
      ],
    })});
    let datas = []
    if (!statistical) return [];
    statistical.student_registers.forEach((data, idx) => {
      const lsStatistical = [];
      data.attendances.forEach(item => lsStatistical.push(`${item.numWeek}${item.numDayinWeek}`))
      const infoStudent = {
        numberOf: idx + 1,
        code: data.code,
        name: data.name,
      }
      range(0, 13).forEach((index) => {
        infoStudent[`less${index}1`] = this.renderCheck(lsStatistical.includes(`${index}1`));
        infoStudent[`less${index}2`] = this.renderCheck(lsStatistical.includes(`${index}2`));
      });
      datas.push(infoStudent);
    });
    return [{ columns, data: datas, title: course.label }];
  }

  getListCourses = () => {
    const { statisticals, lsClass } = this.props;
    const dataCourses = [];
    const columns = [
      {
        title: 'No.',
        width: 68,
        dataIndex: 'numberOf',
        rowKey: uuidv4(),
        align: 'center',
        fixed: 'left',
      },
      {
        title: 'Code',
        width: 96,
        dataIndex: 'code',
        rowKey: uuidv4(),
        align: 'center',
        fixed: 'left',
      },
      {
        title: 'Full name',
        width: 160,
        dataIndex: 'name',
        rowKey: uuidv4(),
        align: 'center',
        fixed: 'left',
      },
    ];

    range(0, 13).forEach((index) => {
      columns.push({
      title: `Week ${index}`,
      rowKey: uuidv4(),
      align: 'center',
      children: [
        {
          title: 'L1',
          dataIndex: `less${index}1`,
          rowKey: uuidv4(),
          width: 100,
          align: 'center',
        },
        {
          title: 'L2',
          dataIndex: `less${index}2`,
          rowKey: uuidv4(),
          width: 100,
          align: 'center',
        },
      ],
    })});
    let datas = []
    statisticals.forEach((data) => {
      data.student_registers.forEach((value, idx) => {
        const lsStatistical = [];
        value.attendances.forEach(item => lsStatistical.push(`${item.numWeek}${item.numDayinWeek}`))
        const infoStudent = {
          numberOf: idx + 1,
          code: value.code,
          name: value.name,
        }
        range(0, 13).forEach((index) => {
          infoStudent[`less${index}1`] = this.renderCheck(lsStatistical.includes(`${index}1`));
          infoStudent[`less${index}2`] = this.renderCheck(lsStatistical.includes(`${index}2`));
        });
        datas.push(infoStudent);
      });
      dataCourses.push({ columns, data: datas, title: data.subject.name });
    })
    return dataCourses;
  }

  renderTableStatistical = () => {
    return this.getCourses().map(({ columns, data, title }) => (
      <div key={uuidv4()} style={{ margin: '21px 28px' }}>
        <StatisticalTable title={title} columns={columns} data={data} />
      </div>
    ));
  }

  getValue = (name, value) => {
    this.setState({ [name]: value });
  }

  handleClick = () => {
    console.log('clicked');
  }

  render() {
    const { lsClass } = this.props;
    const { classValue, subject } = this.state;
    let opptions = optionsClass_1
    if (localStorage.getItem('role') === 'giaovu') {
      opptions = optionsClass
    }
    return (
      <NavBar titleHeader="Statistical">
        <div className="choose-class-wrap">
          <div className="select-wrap" style={{ marginRight: 32 }}>
            <div className="title" />
            <Button onClick={this.handleClick} className="button-down" type="primary" icon="download" size="default">
              Xuất file
            </Button>
          </div>
          <div className="select-wrap" style={{ marginRight: 32 }}>
            <div className="title">Chọn lớp học</div>
            <div className="select-box">
              <SelectBox
                options={opptions}
                valueProps={classValue}
                getValue={value => this.getValue('classValue', value)}
              />
            </div>
          </div>
          <div className="select-wrap">
            <div className="title">Chọn môn học</div>
            <div className="select-box">
              <SelectBox
                options={lsClass}
                valueProps={subject || lsClass[0]}
                getValue={value => this.getValue('subject', value)}
              />
            </div>
          </div>
        </div>
        <div className="table-statistical-wrap">
          {this.renderTableStatistical()}
        </div>
      </NavBar>
    );
  }
}

const mapStateToProps = state => ({
  statisticals: state.statistical.statisticals,
  lsClass: state.statistical.lsClass,
});

const mapDispatchToProps = dispatch => ({
  actionGetAllStatistical: className => dispatch(
    getAllStatistical(className),
  ),
});

Statistical.propTypes = {
  statisticals: array.isRequired,
  lsClass: array.isRequired,
  actionGetAllStatistical: func.isRequired,
};

export default connect(mapStateToProps, mapDispatchToProps)(Statistical);
