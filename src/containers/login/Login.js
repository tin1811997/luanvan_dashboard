import React, { Component } from 'react';
import { object } from 'prop-types';
import { withRouter } from 'react-router-dom';
import './login.scss'

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: null,
      password: null,
      formError: {
        username: "",
        password: ""
      }
    };
  }

  componentDidMount() {
    const { history } = this.props;
    const name = localStorage.name;
    if (name) history.push("/");
  }

  loginSubmit = e => {
    const { history } = this.props;
    e.preventDefault();
    if (this.state.username === "1553033" && this.state.password === "123") {
      localStorage.setItem('name', 'Nguyễn Trung Tín');
      localStorage.setItem('id', '2db0f445-da08-4e95-9ad3-791a174177c4');
      localStorage.setItem('role', 'hocsinh');
      localStorage.setItem('class', '15CLC');
      history.push(`/calenda-view`)
    }
    else if (this.state.username === "ptbhue" && this.state.password === "123") {
      localStorage.setItem('name', 'Phạm Thị Bạch Huệ');
      localStorage.setItem('id', '2db0f445-da08-4e95-9ad3-791a174177c4');
      localStorage.setItem('role', 'giaovu');
      history.push(`/calenda-view`)
    }
  }

  handleChange = e => {
    e.preventDefault();
    const { name, value } = e.target;
    switch (name) {
      case "username":
        this.setState({ username: value })
        break;
      case "password":
        this.setState({ password: value })
        break;
      default:
        break;
    }
  }

  render() {
    return (
      <div className="login">
        <div className="login__header">
          <div className="login__header-top"></div>
          <div className="login__header-content">
            <div className="wrap-content-head">
              <p>TRANG CHỦ</p>
              <div className="login__header-icon">
                <i class="fas fa-home"></i>
                Trang chủ
              </div>
            </div>
        </div>
        </div>
        <form className="login-form">
          <h2 className="login-form__title">User Log In</h2>
          <div className="form-group">
            <label className="label-login" for="name"><strong>Username:</strong></label>
            <input className="form-control" id="name" type="text" placeholder="Name" name="username" onChange={this.handleChange} />
          </div>
          <div className="form-group">
            <label className="label-login" for="password"><strong>Password:</strong></label>
            <input className="form-control" id="password" type="password" placeholder="Password" name="password" onChange={this.handleChange} />
          </div>
          <div className="btn-wrap">
            <button className="btn-login btn-default" type="submit" onClick={this.loginSubmit} >Login</button>
            <button className="btn-cancel" type="submit">Cancel</button>
          </div>
          <label className="login-form__remember">
            <input className="check" type="checkbox" />
            Remember Login
          </label>
          <button className="btn-reset-password" type="submit">Reset Password</button>
        </form>
      </div>
    );
  }
}

Login.propTypes = {
  history: object.isRequired,
};

export default withRouter(Login);