const styles = () => ({
  root: {
    width: '100%',
    fontFamily: 'Roboto',
    border: '1px solid #CCCCCC',
  },
  table: {
    width: '100%',
    boxSizing: 'border-box',
  },
  tableWrapper: {
    overflowX: 'auto',
  },
});

export default styles;
