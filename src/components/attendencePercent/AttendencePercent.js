import React, { Component } from 'react';
import uuidv4 from 'uuid/v4';
import Chart from './chart/Chart';

import './AttendencePercent.scss';

const data = [
  {
    "course_id": "adasd",
    "name": "Toan",
    "percent": 70,
  },
  {
    "course_id": "sdsdffs",
    "name": "Van",
    "percent": 80,
  },
  {
    "course_id": "adasfsd",
    "name": "Ly",
    "percent": 90,
  },
  {
    "course_id": "adassssd",
    "name": "Hoa",
    "percent": 100,
  },
  {
    "course_id": "adassssd",
    "name": "English",
    "percent": 85,
  }
];

class AttendencePercent extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  getDataChart = () => data.map(item => ({
    name: item.name,
    data: {
      labels: [
        'Tham gia',
        'Vắng mặt',
      ],
      datasets: [{
        data: [item.percent, 100 - item.percent],
        backgroundColor: [
          '#36A2EB',
          'red',
        ],
        hoverBackgroundColor: [
          '#36A2EB',
          'red',
        ]
      }],
    }
  }));

  renderChar = () => {
    const datas = this.getDataChart();
    return datas.map(item => (
      <div key={uuidv4()} className="chart-wrap">
        <Chart data={item.data} name={item.name} />
      </div>
    ))
  }

  render() {
    return (
      <div className="attendence-percent-container">
        {this.renderChar()}
      </div>
    );
  }
}

export default AttendencePercent;
