import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { object } from 'prop-types';
import range from 'lodash/range';
import get from 'lodash/get';

import { withStyles } from '@material-ui/core/styles';
import {
  Check, Clear
} from '@material-ui/icons';

import schedule from '../../apis/schedule/schedule';

import styles from './styles';
import './Detail.scss';
import StatisticalTable from '../statisticalTable/StatisticalTable';

class Detail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      registers: [],
      subjectName: '',
    };
  }

  componentDidMount() {
    this.getASchedule();
  }

  getASchedule = async () => {
    // const { location } = this.props;
    // const params = new URLSearchParams(location.search);
    // const scheduleId = params.get('id');
    const res = await schedule.getASchedule('ad160086-d09e-4a5c-ab15-e9b039d3d8b7');
    const { isError, data } = res;
    if (isError) return;
    this.setState({ subjectName: get(data, 'data.subject.name', ''), registers: get(data, 'data.student_registers', []) });
  }

  renderCheck = value => (
    <div className="icon-wrap">
      {this.renderIcon(value)}
    </div>
  );

  renderIcon = (value) => {
    if (value) return <Check className="icon-check" />;
    return '';
  }

  getInfo = () => {
    const { registers } = this.state;

    const columns = [
      {
        title: 'No.',
        width: 68,
        dataIndex: 'numberOf',
        rowKey: 'numberOf',
        align: 'center',
        fixed: 'left',
      },
      {
        title: 'Code',
        width: 96,
        dataIndex: 'code',
        rowKey: 'code',
        align: 'center',
        fixed: 'left',
      },
      {
        title: 'Full name',
        width: 160,
        dataIndex: 'name',
        rowKey: 'name',
        align: 'center',
        fixed: 'left',
      },
    ];

    range(0, 13).forEach((index) => {
      columns.push({
      title: `Week ${index}`,
      rowKey: `Week ${index}`,
      align: 'center',
      children: [
        {
          title: 'L1',
          dataIndex: `less${index}1`,
          rowKey: `less${index}1`,
          width: 100,
          align: 'center',
        },
        {
          title: 'L2',
          dataIndex: `less${index}2`,
          rowKey: `less${index}2`,
          width: 100,
          align: 'center',
        },
      ],
    })});
    let datas = []
    registers.forEach((data, idx) => {
      const lsStatistical = [];
      data.attendances.forEach(item => lsStatistical.push(`${item.numWeek}${item.numDayinWeek}`))
      const infoStudent = {
        numberOf: idx + 1,
        code: data.code,
        name: data.name,
      }
      range(0, 13).forEach((index) => {
        infoStudent[`less${index}1`] = this.renderCheck(lsStatistical.includes(`${index}1`));
        infoStudent[`less${index}2`] = this.renderCheck(lsStatistical.includes(`${index}2`));
      });
      datas.push(infoStudent);
    });
    return { columns, data: datas };
  }

  render() {
    const { subjectName, registers } = this.state;
    const { columns, data } = this.getInfo();
    return (
      <div className="user-deny-container">
        <StatisticalTable title={subjectName} columns={columns} data={data} />
      </div>
    );
  }
}

Detail.propTypes = {
  classes: object.isRequired,
  location: object.isRequired,
};

export default withRouter(withStyles(styles)(Detail));
