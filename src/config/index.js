import merge from 'lodash/merge';
import BASE from './base.conf.json';
import DEV from './dev.conf.json';
import STG from './stag.conf.json';
import PROD from './prod.conf.json';

// FIX ME
// eslint-disable-next-line
let conf = BASE;
switch (true) {
  case window.location.hostname.indexOf('localhost') > -1:
  case window.location.hostname.indexOf('dev-app.isensegroup.com') > -1:
    conf = merge(conf, DEV);
    break;
  case window.location.hostname.indexOf('http://stg-app.isensegroup.com/') > -1:
    conf = merge(conf, STG);
    break;
  case window.location.hostname.indexOf('') > -1:
    conf = merge(conf, PROD);
    break;
  default:
    conf = merge(conf, DEV);
    break;
}
export default conf;
