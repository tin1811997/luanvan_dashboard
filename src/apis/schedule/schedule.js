import HTTPService from '../../services/HTTPServices';

const schedule = {
  getASchedule: scheduleId => HTTPService.sendRequest(
    'get',
    `/attendances/listCourse/${scheduleId}`,
  ),
};

export default schedule;
