import React from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';

import CalenderView from '../calenderView/CalenderView';
import ScheduleView from '../scheduleView/ScheduleView';
import Login from '../login/Login';
import Profile from '../profile/Profile';
import Timeline from '../timeline/Timeline';
import Chart from '../chart/Chart';
import Statistical from '../statistical/Statistical';
import SelectBox from '../../components/selectBox/SelectBox';

function PrivateRoute(props) {
  const name = localStorage.name;
  if (!name) return <Redirect to={{ pathname: '/login' }} />;
  return <props.component {...props} key={props.location.pathname} />;
}

export default (
  <main>
    <Switch>
      <Route exact path="/login" component={Login} />
      <Route exact path="/profile" component={Profile} />
      <Route exact path="/select" component={SelectBox} />
      <PrivateRoute exact path="/" component={CalenderView} />
      <PrivateRoute exact path="/calenda-view" key="calenda-page" component={CalenderView} />
      <PrivateRoute exact path="/schedule" component={ScheduleView} />
      <PrivateRoute exact path="/timeline" component={Timeline} />
      <PrivateRoute exact path="/chart" component={Chart} />
      <PrivateRoute exact path="/statistical" component={Statistical} />
    </Switch>
  </main>
);
