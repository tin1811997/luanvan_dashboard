import React, { Component } from 'react';
import NavBar from '../../components/navBar/NavBar';
import Calendar from '../../components/calendar/Calendar';

class CalenderView extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <NavBar titleHeader="Calendar">
        <Calendar />
      </NavBar>
    );
  }
}

export default CalenderView;