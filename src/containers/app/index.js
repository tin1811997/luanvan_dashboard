import React from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import routes from './routers';

const App = () => (
  <Router>
    {routes}
  </Router>
);

export default App;
