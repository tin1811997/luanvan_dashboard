import React, { Component } from 'react';
import { Link, withRouter } from "react-router-dom";
import { withStyles } from "@material-ui/core/styles";

import classNames from "classnames";
import Drawer from '@material-ui/core/Drawer';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import List from '@material-ui/core/List';
import CssBaseline from '@material-ui/core/CssBaseline';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import ListItem from '@material-ui/core/ListItem';
import Loading from "../Common/Loading";
import Account from '../../components/Account/Account';
import logo from '../../assets/img/logo.png'

import styles from './styles';

class NavBar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: true,
      loading: false,
    };
  }

  componentDidMount() {
    // this.setState({ loading: true });
    // this.interval = setInterval(() => {
    //   this.setState({ loading: false });
    //   clearInterval(this.interval);
    // }, 2000);
  }

  handleDrawerOpen = () => {
    this.setState({ open: true });
  }

  handleDrawerClose = () => {
    this.setState({ open: false });
  }

  render() {
    const { classes, children, titleHeader } = this.props;
    const { open, loading } = this.state;
    return (
      <div className={classes.root}>
      <CssBaseline />
      <AppBar
        position="fixed"
        className={classNames(classes.appBar, {
          [classes.appBarShift]: open,
        })}
        style={{ boxShadow: 'none' }}
      >
        <Toolbar style={{ paddingLeft: 0, minHeight: 56, backgroundColor: '#30add1' }}>
          <IconButton
            color="inherit"
            aria-label="Open drawer"
            onClick={this.handleDrawerOpen}
            edge="start"
            className={classNames(classes.menuButton, {
              [classes.hide]: open,
            })}
          >
            <MenuIcon />
          </IconButton>
          <div><img src={logo} alt="logo" className={classes.imgLogo}></img></div>
          <Typography className={classes.titleHeader} variant="h6" noWrap>
            {titleHeader}
          </Typography>
        </Toolbar>
      </AppBar>
      <Drawer
        variant="permanent"
        className={classNames(classes.drawer, {
          [classes.drawerOpen]: open,
          [classes.drawerClose]: !open,
        })}
        classes={{
          paper: classNames({
            [classes.drawerOpen]: open,
            [classes.drawerClose]: !open,
          }),
        }}
        open={open}
      >
        <div className={classes.toolbar}>
          <IconButton onClick={this.handleDrawerClose}>
            {!open ? <ChevronRightIcon style={{ color: '#000' }} /> : <ChevronLeftIcon style={{ color: '#000' }} />}
          </IconButton>
        </div>
        <Divider />
        <List>
          <ListItem button>
            <Link to="/calenda-view" className={classes.itemMenu}>
              <div>
                <span className={`${classes.icon} fas fa-calendar-alt`} />
              </div>
              <div className={classes.itemMenuContent}>Calendar</div>
            </Link>
          </ListItem>
          <ListItem button>
            <Link to="/timeline" className={classes.itemMenu}>
              <div>
                <span className={`${classes.icon} fas fa-clipboard-list`} />
              </div>
              <div className={classes.itemMenuContent}>Schedules</div>
            </Link>
          </ListItem>
          <ListItem button>
            <Link to="/chart" className={classes.itemMenu}>
              <div>
                <span className={`${classes.icon} fas fa-chart-pie`} />
              </div>
              <div className={classes.itemMenuContent}>Attendence percent</div>
            </Link>
          </ListItem>
          <ListItem button>
            <Link to="/statistical" className={classes.itemMenu}>
              <div>
                <span className={`${classes.icon} fas fa-file-excel`} />
              </div>
              <div className={classes.itemMenuContent}>Statistical</div>
            </Link>
          </ListItem>
        </List>
        <div className={classes.drawerAccount}>
          <Account />
        </div>
      </Drawer>
      <main className={classes.content}>
        {loading ? <Loading /> : children}
      </main>
    </div>
    );
  }
}

export default withRouter(withStyles(styles)(NavBar));