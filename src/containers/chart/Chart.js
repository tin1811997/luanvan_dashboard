import React, { Component } from 'react';
import AttendencePercent from '../../components/attendencePercent/AttendencePercent';
import NavBar from '../../components/navBar/NavBar';

class Chart extends Component {
  render() {
    return (
      <NavBar titleHeader="Attendence percent">
        <AttendencePercent />
      </NavBar>
    );
  }
}

export default Chart;
