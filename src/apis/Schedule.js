import HTTPService from '../services/HTTPServices';

const ScheduleAPI = {
  getAllSchedule: (id) => HTTPService.sendRequest('get', '/schedules/web/' + id)
};

export default ScheduleAPI;
