import React, { Component } from 'react';
import {Pie} from 'react-chartjs-2';

import './Chart.scss';
import { object, string } from 'prop-types';

class Chart extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { name, data, legendOpts } = this.props;
    return (
      <div className="chart-container">
        <div className="course-name">{name}</div>
        <Pie data={data} legend={legendOpts} redraw />
      </div>
    );
  }
}

Chart.propTypes = {
  data: object.isRequired,
  legendOpts: object,
  name: string,
};

Chart.defaultProps = {
  name: 'Course name',
  legendOpts: {
    display: true,
    position: 'bottom',
    fullWidth: true,
    reverse: false,
    labels: {
      fontColor: '#000000'
    }
  },
}

export default Chart;
