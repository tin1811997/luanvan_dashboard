const styles = theme => ({
  overlay: {
    display: "flex",
    backgroundColor: "rgba(0, 0, 0, 0.71)",
    width: "100vw",
    height: "100vh",
    position: "fixed",
    top: "0",
    left: "0",
    zIndex: 9000,
    justifyContent: 'center',
    alignItems: 'center'

  }
})
export default styles;
