import React from "react";
import ReactLoading from "react-loading";
import { withStyles } from '@material-ui/core/styles';

import styles from './Loading.styles';

const Loading = ({ classes }) => (
  <div className={classes.overlay}>
    <div>
      <ReactLoading type="bars" color="#10b89c" />
    </div>
  </div>
);

export default withStyles(styles)(Loading);
