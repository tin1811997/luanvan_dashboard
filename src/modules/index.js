import { combineReducers } from 'redux';
import statistical from './statistical';

export default combineReducers({
  statistical,
});
