import React from 'react';
import { object, number } from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';

import TableHeader from '../TableHeader/TableHeader';

import styles from './styles';
import Row from '../Row/Row';

const uuidv4 = require('uuid/v4');

class TableComponent extends React.Component {
  state = {};

  render() {
    const {
      classes,
      dataTable: { colums, rows },
      pageSize,
      page,
    } = this.props;
    const tableHeader = [];
    const tableColums = [];
    const headerWidth = [];
    colums.forEach(({ label, width, name }) => {
      tableHeader.push(label);
      tableColums.push(name);
      headerWidth.push(width);
    });
    const emptyRows = pageSize - Math.min(pageSize, rows.length - page * pageSize);
    return (
      <div className={classes.root}>
        <div className={classes.tableWrapper}>
          <Table className={classes.table} aria-labelledby="tableTitle">
            <TableHeader tableHeader={tableHeader} headerWidth={headerWidth} />
            <TableBody>
              {rows
                .slice(page * pageSize, page * pageSize + pageSize)
                .map(item => (
                  <Row
                    key={uuidv4()}
                    item={item}
                    colums={tableColums}
                    handleClick={() => {}}
                  />
                ))}
              {emptyRows > 0 && (
                <TableRow style={{ height: 49 * emptyRows }}>
                  <TableCell colSpan={6} />
                </TableRow>
              )}
            </TableBody>
          </Table>
        </div>
      </div>
    );
  }
}

TableComponent.propTypes = {
  classes: object.isRequired,
  dataTable: object.isRequired,
  page: number,
  pageSize: number,
};

TableComponent.defaultProps = {
  page: 0,
  pageSize: 4,
}

export default withStyles(styles)(TableComponent);
