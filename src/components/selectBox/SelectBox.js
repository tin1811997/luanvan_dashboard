import React, { Component } from 'react';
import { array, func, object } from 'prop-types';
import Select from 'react-select';

const customSelect = {
  dropdownIndicator: provided => ({
    ...provided,
    borderColor: '#555555 transparent transparent',
    borderStyle: 'solid',
    borderWidth: '5.6px 5.6px 2.5px',
    display: 'inline-block',
    height: '0',
    width: '0',
    padding: 0,
    position: 'relative',
    overflow: 'hidden',
    margin: 8,
  }),
  indicatorSeparator: () => ({ display: 'none' }),
};

class SelectBox extends Component {
  constructor(props) {
    super(props);
    this.state = {
      valueSelect: {},
    };
  }

  componentDidMount() {
    const { valueProps } = this.props;
    this.setState({ valueSelect: valueProps });
  }

  static getDerivedStateFromProps(nextProps) {
    const valueSelect = nextProps.valueProps;
    return {
      valueSelect,
    };
  }

  onChangeSelect = (valueSelect) => {
    const { getValue } = this.props;
    getValue(valueSelect);
    this.setState({ valueSelect });
  }

  render() {
    const { options } = this.props;
    const { valueSelect } = this.state;
    return (
      <Select
        options={options}
        styles={customSelect}
        value={valueSelect}
        classNamePrefix="select-company"
        placeholder="Select company"
        onChange={this.onChangeSelect}
      />
    );
  }
}

SelectBox.propTypes = {
  options: array,
  getValue: func,
  valueProps: object,
};

SelectBox.defaultProps = {
  options: [],
  getValue: () => {},
  valueProps: {},
};

export default SelectBox;