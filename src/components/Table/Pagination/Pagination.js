/* eslint-disable radix */
import React, { Component } from 'react';
import { array, number, func } from 'prop-types';

import './Pagination.scss';

const uuidv4 = require('uuid/v4');

export default class Pagination extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      currentPage: null,
      pageCount: null,
    };
  }

  componentWillMount() {
    const { startingPage, data, pageSize } = this.props;
    const currentPage = startingPage || 1;
    let pageCount = parseInt(data.length / pageSize);
    if (data.length % pageSize > 0) {
      pageCount += 1;
    }
    this.setState({
      currentPage,
      pageCount,
    });
  }

  setCurrentPage(num) {
    const { handleChangePage } = this.props;
    handleChangePage(num);
    this.setState({ currentPage: num });
  }

  createControls() {
    const { pageCount, currentPage } = this.state;
    const controls = [];
    for (let i = 1; i <= pageCount; i += 1) {
      const baseClassName = 'pagination-controls__button';
      const activeClassName = i === currentPage ? `${baseClassName}--active` : '';
      controls.push(
        <div
          key={uuidv4()}
          className={`${baseClassName} ${activeClassName}`}
          onClick={() => this.setCurrentPage(i)}
        >
          {i}
        </div>,
      );
    }
    return controls;
  }

  createPaginatedData() {
    const { data, pageSize } = this.props;
    const { currentPage } = this.state;
    const upperLimit = currentPage * pageSize;
    const dataSlice = data.slice((upperLimit - pageSize), upperLimit);
    return dataSlice;
  }

  render() {
    const { currentPage, pageCount } = this.state;
    const { data, pageSize, emptyRows } = this.props;
    return (
      <div className="pagination-container">
        <div className="pagination-controls">
          <div className="show-item-in-page">
            Showing&nbsp;
            {currentPage * pageSize - emptyRows}
            &nbsp;of&nbsp;
            {data.length}
          </div>
          <div className="change-page-previous-wrap">
            <div
              className={
                currentPage === 1
                  ? 'disable-btn btn-first' : 'pagination-controls__button btn-first'
              }
              onClick={() => this.setCurrentPage(1)}
            >
              &lt;&lt;&nbsp;First page
            </div>
            <div
              className={
                currentPage === 1
                  ? 'disable-btn btn-previous' : 'pagination-controls__button btn-previous'
              }
              onClick={
                currentPage > 1
                  ? () => this.setCurrentPage(currentPage - 1)
                  : () => this.setCurrentPage(currentPage)
              }
            >
              &lt;&nbsp;Previous
            </div>
          </div>
          {this.createControls()}
          <div className="change-page-next-wrap">
            <div
              className={
                currentPage === pageCount
                  ? 'disable-btn btn-next' : 'pagination-controls__button btn-next'
              }
              onClick={
                currentPage < pageCount
                  ? () => this.setCurrentPage(currentPage + 1)
                  : () => this.setCurrentPage(currentPage)
              }
            >
              Next&nbsp;&gt;
            </div>
            <div
              className={
                currentPage === pageCount
                  ? 'disable-btn btn-last' : 'pagination-controls__button btn-last'
              }
              onClick={() => this.setCurrentPage(pageCount)}
            >
              Last page&nbsp;&gt;&gt;
            </div>
          </div>
        </div>
      </div>
    );
  }
}

Pagination.propTypes = {
  data: array.isRequired,
  pageSize: number.isRequired,
  startingPage: number.isRequired,
  handleChangePage: func.isRequired,
  emptyRows: number.isRequired,
};
