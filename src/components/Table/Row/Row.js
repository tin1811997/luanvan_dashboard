import React, { Component } from 'react';

import TableRow from '@material-ui/core/TableRow';
import TableCell from '@material-ui/core/TableCell';
import { object, func, array } from 'prop-types';

const uuidv4 = require('uuid/v4');

class Row extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isSelected: false,
    };
  }

  handleClick = (item) => {
    const { handleClick } = this.props;
    const { isSelected } = this.state;
    handleClick(item);
    this.setState({ isSelected: !isSelected });
  }

  renderTableCell = () => {
    const { item, colums } = this.props;
    return colums.map(key => (
      <TableCell key={uuidv4()} align="left" padding="checkbox">
        {item[key]}
      </TableCell>
    ));
  }

  renderTableRow = () => {
    const { item } = this.props;
    const { isSelected } = this.state;
    return (
      <TableRow
        hover
        onClick={() => this.handleClick(item)}
        role="checkbox"
        aria-checked={isSelected}
        tabIndex={-1}
        selected={isSelected}
      >
        {this.renderTableCell()}
      </TableRow>
    );
  }

  render() {
    return (
      <>
        {this.renderTableRow()}
      </>
    );
  }
}

Row.propTypes = {
  item: object.isRequired,
  handleClick: func.isRequired,
  colums: array.isRequired,
};

export default Row;
