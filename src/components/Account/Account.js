import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { object } from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import classnames from 'classnames';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import Collapse from '@material-ui/core/Collapse';
import Avatar from '@material-ui/core/Avatar';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Tooltip from '@material-ui/core/Tooltip';

import styles from './styles';

class Account extends Component {
  constructor(props) {
    super(props);
    this.state = {
      expanded: true,
    };
  }

  handleExpandClick = () => {
    this.setState(state => ({ expanded: !state.expanded }));
  };

  logOut = () => {
    const { history } = this.props;
    localStorage.removeItem('name');
    history.push('/login');
  }

  profile = () => {
    const { history } = this.props;
    history.push('/profile');
  }

  render() {
    const { classes } = this.props;
    const { expanded } = this.state;
    const name = localStorage.getItem('name');
    return (
      <Card className={classes.card}>
        <Collapse in={!expanded} timeout="auto" unmountOnExit className={classes.collapse}>
          <div onClick={this.profile} className={classes.optionRow}>
            Profile
          </div>
        </Collapse>
        <Collapse in={!expanded} timeout="auto" unmountOnExit className={classes.collapse}>
          <div onClick={this.logOut} className={classes.optionRow}>
            Thoát
          </div>
        </Collapse>
        <CardActions className={classes.actions} disableSpacing onClick={this.handleExpandClick}>
          <Avatar aria-label="Recipe" className={classes.avatar}>
            {name[0]}
          </Avatar>
          <Tooltip title={`${name}`}>
            <div className={classes.userName}>
              {name}
            </div>
          </Tooltip>
          <div
            className={classnames(classes.expand, {
              [classes.expandOpen]: expanded,
            })}
          >
            <ExpandMoreIcon />
          </div>
        </CardActions>
      </Card>
    );
  }
}

Account.propTypes = {
  classes: object.isRequired,
  history: object.isRequired,
};

export default withRouter(withStyles(styles)(Account));
