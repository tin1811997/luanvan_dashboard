import React, { Component } from 'react';
import NavBar from '../../components/navBar/NavBar';
import Detail from '../../components/detail/Detail';

class ScheduleView extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <NavBar titleHeader="Course detail">
        <Detail />
      </NavBar>
    );
  }
}

export default ScheduleView;