import React, { Component } from 'react';
import { object } from 'prop-types';
import { withRouter } from 'react-router-dom';
import BigCalendar from 'react-big-calendar';
import moment from 'moment';

import 'react-big-calendar/lib/css/react-big-calendar.css';
import ScheduleAPI from '../../apis/Schedule';

class Calendar extends Component {
  state = {
    schedules: [],
  };

  componentDidMount() {
    this.getAllSchedule();
  }

  getAllSchedule = async () => {
    const id = localStorage.getItem('id')
    let res = await ScheduleAPI.getAllSchedule(id);
    const { isError, data } = res;
    if (isError) return;
    this.setState({
      schedules: data.data,
    });
  };

  modifySchedules = schedules => {
    return schedules.map(item => ({
      title: `(${item.course.name}${item.course.order}${
        item.course.course_type
      })${item.room.name}`,
      start: new Date(item.date),
      end: new Date(item.date),
      id: item.id
    }));
  };

  render() {
    const { history } = this.props;
    const localizer = BigCalendar.momentLocalizer(moment);
    return (
      <div style={{ height: '85vh', width: '100%' }}>
        <BigCalendar
          selectable
          localizer={localizer}
          startAccessor="start"
          endAccessor="end"
          events={this.modifySchedules(this.state.schedules)}
          onSelectEvent={event => history.push(`/schedule?id=${event.id}`)}
        />
      </div>
    );
  }
}

Calendar.propTypes = {
  history: object.isRequired,
};

export default withRouter(Calendar);
