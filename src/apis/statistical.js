import HTTPService from '../services/HTTPServices';

const Statistical = {
  getAllStatistical: className => HTTPService.sendRequest(
    'get',
    `/attendances/listCourse?class=${className}`,
  ),
};

export default Statistical;
